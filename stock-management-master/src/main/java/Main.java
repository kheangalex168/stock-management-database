import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class Main {
    static int perPage = 5;
    static int page = 1;
    static List<Product> productList = new ArrayList<>();
    static Connection conn = DB.connect();

    public static void main(String[] args) throws IOException {
        Header();
        while (true) {
            String menuChoice = H.getChoice();
            switch (menuChoice) {
                case "*" -> displayProduct(page);
                case "W", "w" -> writeProduct();
                case "R", "r" -> readProduct();
                case "U", "u" -> updateProduct();
                case "D", "d" -> deleteProduct();
                case "F", "f" -> moveToFirst();
                case "P", "p" -> moveToPrevious();
                case "N", "n" -> moveToNext();
                case "L", "l" -> moveToLast();
                case "G", "g" -> gotoPage();
                case "S", "s" -> searchWildcard();
                case "SE", "se", "Se", "sE" -> setRow();
                case "SA", "sa", "Sa", "sA" -> saveToDatabase();
                case "RE", "re", "Re", "rE" -> restore();
                case "BA", "ba", "Ba", "bA" -> backup();
                case "H", "h" -> System.out.println("Not yet implement!");
                case "E", "e" -> exit();
            }
            printMainMenu();
        }
    }

    private static void saveToDatabase() {
        String choice = H.getString("Do you want to save it? [Y/y] or [N/n] : ");
        if (choice.equalsIgnoreCase("y")){
            try {
                Statement statement = conn.createStatement();
                String sql =
                        "insert into \"tblProduct\" (id, name, unit_price, qty) values(?,?,?,?)";
                statement.execute("truncate only \"tblProduct\" restart identity");
                PreparedStatement preparedStatement = conn.prepareStatement(sql);
                for (Product product : productList) {
                    if (product.isNotDelete()){
                        preparedStatement.setInt(1, product.getID());
                        preparedStatement.setString(2, product.getName());
                        preparedStatement.setFloat(3, (float) product.getUnitPrice());
                        preparedStatement.setInt(4, product.getQty());
                        preparedStatement.execute();
                        product.setNotUpdate(true);
                        product.setNotDelete(true);
                    }
                }
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        H.printWaveStyle(24);
        System.out.println("\tSave successfully!");
        H.printWaveStyle(24);
    }

    private static void backup() {
        System.out.println("Not yet Implement!");
    }

    private static void restore() {
        System.out.println("Not yet Implement!");
    }

    private static void exit() {
        System.out.println("Good Bye !!!");
        System.exit(0);
    }

    private static void setRow() {
        perPage = H.getInt("Please enter row for Display: ", 1, Integer.MAX_VALUE);
        ListOperation.readNotDeleteDataFromList(
                Const.column,
                perPage,
                Main.page
        );
        H.printWaveStyle(25);
        System.out.printf("\tSet row to %d Successfully%n", Main.perPage);
        H.printWaveStyle(25);
    }

    private static void searchWildcard() {
        String productName = H.getString("Search By Name: ").toLowerCase();
        for (Product product : Main.productList) {
            if (product.getName().toLowerCase().contains(productName)){
                H.productPreview(product);
            }
        }
    }

    private static void gotoPage() {
        Main.page = H.getInt("Go to Page: ", 1, Integer.MAX_VALUE);
        ListOperation.readNotDeleteDataFromList(Const.column, perPage, Main.page);
    }

    private static void moveToNext() {
        int row = productList.size();
        ++page;
        int currentPage = row % perPage == 0 ? row / perPage : (row / perPage) + 1;

        if (currentPage < page) {
            ListOperation.readNotDeleteDataFromList(
                    Const.column,
                    perPage,
                    --page
            );
        } else {
            ListOperation.readNotDeleteDataFromList(
                    Const.column,
                    perPage,
                    page
            );
        }
    }

    private static void moveToLast() {
//        allRow % perPage
        int row = productList.size();
        Main.page = row % perPage == 0 ? row / perPage : (row / perPage) + 1;
        ListOperation.readNotDeleteDataFromList(
                Const.column,
                perPage,
                Main.page
        );
    }

    private static void moveToPrevious() {
        Main.page = (Main.page != 1) ? --Main.page : 1;
        ListOperation.readNotDeleteDataFromList(
                Const.column,
                perPage,
                page
        );
    }

    private static void readProduct() throws FileNotFoundException {
        ListOperation.printProductByID("Read by ID : ");
    }

    private static void writeProduct() throws IOException {
        ListOperation.addRecord(H.productPreview(tempNewProduct()));
    }

    private static void displayProduct(int page) {
        ListOperation.readNotDeleteDataFromList(Const.column, perPage, page);
    }

    private static void moveToFirst() {
        displayProduct(1);
    }

    private static void deleteProduct() {
        int indexByID  = ListOperation
                .getProductIndexByID(H.getInt("Please input ID of Product : ",1,Integer.MAX_VALUE));
        H.productPreview(productList.get(indexByID));
        String str = H.getString("Are you sure want to add this record? [Y/y] or [N/n] ");
        if (str.equalsIgnoreCase("y")) {
            Product product = productList.get(indexByID);
            product.setNotDelete(false);
            productList.set(indexByID, product);
            H.printWaveStyle(25);
            System.out.println("\tProduct was removed.");
            H.printWaveStyle(25);

        }
    }

    private static void updateProduct() throws IOException {
        int index = ListOperation.getProductIndexByID(H.getInt("Please input ID of Product : ",1,Integer.MAX_VALUE));
        Product product = H.productPreview(productList.get(index));
        System.out.println("What do you want to update");
        System.out.println("╔═════════════════════════════════════════════════════════════════════╗");
        System.out.println("║  1.All   | 2.Name  | 3.Quantity | 4.Unit Price  5. Back to menu     ║");
        System.out.println("╚═════════════════════════════════════════════════════════════════════╝");
        int updateChoice = H.getInt("Option (1-5) : ", 1, 5);
        String strID = String.valueOf(product.getID());
        switch (updateChoice) {
            case 1:
                ListOperation.updateFullRecord(product, index);
                break;
            case 2:
                ListOperation.updateRecordName(product, index);
                break;
            case 3:
                ListOperation.updateRecordQty(product, index);
                break;
            case 4:
                ListOperation.updateRecordUnitPrice(product, index);
                break;
            default:
                break;
        }
    }

    private static void printMainMenu() {
        System.out.println("╔══════════════════════════════════════════════════════════════════════════════════════════════╗");
        System.out.println("║  *)Display | W)write | R)ead    | U)pdate | D)elete | F)irst    | P)revious | N)ext | L)ast  ║");
        System.out.println("║  S)earch   | G)o to  | Se)t row | Sa)ve   | B)ackup | Re)store  | H)elp     | E)xit          ║");
        System.out.println("╚══════════════════════════════════════════════════════════════════════════════════════════════╝");
    }


    private static Product tempNewProduct() throws IOException {
        Product product = new Product();
        product.setID(ListOperation.getLastID()+1);
        product.setName(H.getString("Product's name : "));
        product.setUnitPrice(H.getDouble("Product's Price : ", 1, Double.MAX_VALUE));
        product.setQty(H.getInt("Product's Qty : ", 0, Integer.MAX_VALUE));
        product.setImportDate(H.getCurrentDate());
        product.setNotDelete(true);
        return product;
    }

    private static void Header() {
        H.printWithTab(12, "Welcome to", true);
        H.printWithTab(11, "Stock Management\n", true);
        H.asciiGenerator(100, 30, "SiemReabG1", 100);
        String text = "\nPlease wait Loading...";
        H.printThread(text + "\n", 100);
        long startTime = System.nanoTime();
        readDataFromDatabase(conn);
        long stopTime = System.nanoTime();
        double measureTime = TimeUnit.SECONDS.convert(stopTime - startTime, TimeUnit.NANOSECONDS);
        System.out.printf("Current time loading : %.3fs%n", measureTime);
        printMainMenu();
    }

    private static void readDataFromDatabase(Connection conn) {
        try {
            Statement statement = Objects
                    .requireNonNull(conn)
                    .createStatement(
                            ResultSet.TYPE_SCROLL_INSENSITIVE,
                            ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = statement.executeQuery("select * from \"tblProduct\"");
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                double unitPrice = rs.getFloat("unit_price");
                int qty = rs.getInt("qty");
                String importDate = rs.getString("imported_date");
                Product product = new Product(
                        id,
                        name,
                        unitPrice,
                        qty,
                        importDate,
                        true,
                        true
                );
                productList.add(product);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
