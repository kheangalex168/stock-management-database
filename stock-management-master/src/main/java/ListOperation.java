import org.nocrala.tools.texttablefmt.BorderStyle;
import org.nocrala.tools.texttablefmt.CellStyle;
import org.nocrala.tools.texttablefmt.ShownBorders;
import org.nocrala.tools.texttablefmt.Table;

import java.io.*;
import java.util.*;


public class ListOperation {

    public static void updateRecordUnitPrice(Product product, int index) {
        double productQty = H.getDouble("Product's Price : ", 1, Integer.MAX_VALUE);
        product.setUnitPrice(productQty);
        product.setNotUpdate(false);
        String str = H.getString("Are you sure want to add update record? [Y/y] or [N/n] ");
        if (str.equalsIgnoreCase("y")) {
            Main.productList.set(index, product);
        }
    }

    public static void updateRecordQty(Product product, int index) {
        int productQty = H.getInt("Stock Quantity : ", 1, Integer.MAX_VALUE);
        product.setQty(productQty);
        product.setNotUpdate(false);
        String str = H.getString("Are you sure want to add update record? [Y/y] or [N/n] ");
        if (str.equalsIgnoreCase("y")) {
            Main.productList.set(index, product);
        }
    }

    public static void updateRecordName(Product product, int index) {
        String productName = H.getString("Product's Name : ");
        product.setName(productName);
        product.setNotUpdate(false);
        String str = H.getString("Are you sure want to add update record? [Y/y] or [N/n] ");
        if (str.equalsIgnoreCase("y")) {
            Main.productList.set(index, product);
        }
    }

    public static void updateFullRecord(Product product, int index) throws IOException {
        String productName = H.getString("Product's Name : ");
        double productPrice = H.getDouble("Product's Price : ", 1, Double.MAX_VALUE);
        int productQty = H.getInt("Stock Quantity : ", 0, Integer.MAX_VALUE);
        product.setNotUpdate(false);
        product.setName(productName);
        product.setUnitPrice(productPrice);
        product.setQty(productQty);
        String str = H.getString("Are you sure want to update this record? [Y/y] or [N/n] ");
        if (str.equalsIgnoreCase("y")) {
            Main.productList.set(index, product);
        }

    }

    public static int getProductIndexByID(int id) {
        List<Product> productList = Main.productList;
        for (int i = 0, productListSize = productList.size(); i < productListSize; i++) {
           if (productList.get(i).getID() == id) return i;
        }
        return -1;
    }

    public static void printProductByID(String text) throws FileNotFoundException {
        int id = H.getInt(text, 1, Integer.MAX_VALUE);
        for (Product product : Main.productList) {
            if (product.getID() == id){
                H.productPreview(product);
            }
        }
    }

    public static void productStrPreview(String separator, String thisLine) {
        Table table = new Table(
                1,
                BorderStyle.UNICODE_BOX_HEAVY_BORDER,
                ShownBorders.SURROUND
        );
        table.setColumnWidth(0, 30, 30);
        String[] thisRow = thisLine.split(separator);
        table.addCell("ID             : " + thisRow[0]);
        table.addCell("Name           : " + thisRow[1]);
        table.addCell("Unit Price     : " + thisRow[2]);
        table.addCell("Qty            : " + thisRow[3]);
        table.addCell("Import Date    : " + thisRow[4]);
        System.out.println(table.render());
    }

    public static void readNotDeleteDataFromList(String[] column, int perPage, int page) {
        int index = 1;

        CellStyle cellStyle = new CellStyle(CellStyle.HorizontalAlign.CENTER);
        Table t = new Table(
                5,
                BorderStyle.UNICODE_BOX_DOUBLE_BORDER);

        t.addCell(column[0], cellStyle);
        t.addCell(column[1], cellStyle);
        t.addCell(column[2], cellStyle);
        t.addCell(column[3], cellStyle);
        t.addCell(column[4], cellStyle);

        t.setColumnWidth(0, 18, 18);
        t.setColumnWidth(1, 18, 18);
        t.setColumnWidth(2, 18, 18);
        t.setColumnWidth(3, 18, 18);
        t.setColumnWidth(4, 18, 18);

        for (Product product : Main.productList) {
            if (product.isNotDelete()){
                if (index >= (perPage * (page - 1)) + 1 && index <= page * perPage) {
                    int id = product.getID();
                    t.addCell(String.valueOf(id), cellStyle);
                    String name = product.getName();
                    t.addCell(name, cellStyle);
                    double unitPrice = product.getUnitPrice();
                    t.addCell(String.format("%.2f", unitPrice), cellStyle);
                    int qty = product.getQty();
                    t.addCell(String.valueOf(qty), cellStyle);
                    String importedDate = product.getImportDate();
                    t.addCell(importedDate, cellStyle);
                }
            }
            if (index > page * perPage) break;
            ++index;
        }
        System.out.println(t.render());
        int allRow = countLine();
        H.printWaveStyle(94);
        int lastPage = allRow % perPage == 0 ? (allRow / perPage) : (allRow / perPage) + 1;
        System.out.printf("Page %d of %d", page, lastPage);
        H.printWithTab(14, " ", false);
        System.out.printf("Total Record: %d%n", allRow);
        H.printWaveStyle(94);
    }

    public static void addRecord(Product product) {
        String str = H.getString("Are you sure want to add this record? [Y/y] or [N/n] ");
        if (str.equalsIgnoreCase("y")) {
                Main.productList.add(product);
                H.printWaveStyle(28);
                System.out.printf("%d was add successfully%n", product.getID());
                H.printWaveStyle(28);
        }
    }

    public static int countLine() {
        return Main.productList.size();
    }

    public static int getLastID() {
        Main.productList.sort(Comparator.comparing(Product::getID));
        return Main.productList.isEmpty() ? 0 : Main.productList.get(Main.productList.size()-1).getID();
    }
}
