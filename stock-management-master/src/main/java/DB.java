import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DB {
    public static Connection connect(){
        Connection connection;
        {
            try {
                connection = DriverManager.getConnection(Const.url, Const.user, Const.password);
                if (connection != null){
                    System.out.println("You are connected!");
                    return connection;
                }else{
                    System.out.println("No connection!");
                    return null;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
